# Inkscape extension removeDuplicateNodes

Remove duplicate nodes from path and join nodes closer than a specified distance. Optionally join start/end nodes if closer than a specified distance.

When joining nodes the new coordinate will be interpolated from the original coordinates.

The extension will appear in Extensions -> Modify Path

Read more about this with some visual examples on [cutlings.wasbo.net](http://cutlings.wasbo.net/remove-duplicate-nodes/)

Changes in version 2:
- Handles situations where more than duplettes (combining multiple nodes if the total length is less  than the specified length)
- When groups of nodes are combined where positions do not match exactly the nodes close to longer segments are somewhat protected to avoid changing shapes.
- Added option to reorder and reverse subpaths so that path-ends of different subpaths can be joined if closer than a specified distance. The sub-paths need to be combined to one path (Ctrl+K) first. This joining can be done by interpolation or by adding straight line segments.

Changes in version 3:
- main part of functionality moved out into method remove_duplicate_nodes
- code style slightly more toward PEP8
- fixed crash with zero-length segment

version 3.1
- fixed bug from 3.0
- fixed interpolation for first/last node when joining separate subpaths into loop
- added unit-testing (not finished)

New name clean_up_path:

I have a merge request for this extension [here](https://gitlab.com/inkscape/extensions/-/merge_requests/499) and I have had some help by the Inkscape team preparing it to be an extension to come with Inkscape, but the process of including it have stalled so I publish the files here. The extension has changed name to clean_up_path. I have kept the old files for now.
Please find the subpath_iterator.txt file and follow the instructions within that too as the paths.py in inkex need an extra method.
