# -*- coding: utf-8 -*-
from removeDuplicateNodes import RemoveDuplicateNodes

#TODO - test other cases - currently only testing if testing works
class RemoveDuplicateNodesTest():#(ComparisonMixin, TestCase):
    effect_class = RemoveDuplicateNodes
    '''
    comparisons = [
        (
        )
    ]
    compare_filters = [
        CompareWithPathSpace(),
        CompareNumericFuzzy(),
    ]
    '''

    def test_simple_loop(self):
        args = ["--id=path_simple_loop",
                "--minUse=False", "--joinEnd=False", "--joinEndSub=True",
                "--maxdist2=4",
                self.data_file("svg", "remove_duplicate_nodes.svg")]
        effect = self.effect_class()
        effect.run(args)
        new_path = effect.svg.getElement("//svg:path").path
        expected_path = effect.svg.getElementById(
            "path_simple_loop_False_False_4").path
        assert new_path == expected_path
